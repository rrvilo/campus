/*
 * Copyright 2020 Steerpath Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, {useState} from 'react';
import {StyleSheet, View, Text, Switch} from 'react-native';
import {Card} from 'react-native-elements';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {black, colors} from '../styles/colors';

export default function LocationSharingScreen() {
  const [enabled, setEnabled] = useState(false);
  let bool = false;

  const toggleSwitch = () => {
    setEnabled(!enabled);
  };

  return (
    <View style={{flex: 1}}>
      <Card>
        <React.Fragment>
          <TouchableOpacity onPress={toggleSwitch}>
            <View style={styles.header}>
              <Text style={styles.title}>Enable location sharing</Text>
              <Switch value={enabled} onValueChange={toggleSwitch} />
            </View>
          </TouchableOpacity>
        </React.Fragment>
      </Card>
    </View>
  );
}

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  title: {fontSize: 14, fontWeight: 'bold', color: colors.black},
  switch: {},
});
