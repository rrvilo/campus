/*
 * Copyright 2020 Steerpath Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import {StyleSheet, View, Text, Linking} from 'react-native';
import {colors} from '../styles/colors';
import {getVersion} from 'react-native-device-info';
import {AboutItem} from '../components/AboutItem';

export default function AboutScreen() {
  const sourceCodeUrl =
    'https://bitbucket.org/steerpath/steerpath-demo-app/src/';
  const steerpathUrl = 'https://steerpath.com';

  const openLink = url => {
    Linking.openURL(url);
  };

  return (
    <View style={styles.container}>
      <Text style={styles.textView}>
        This application is a minimal example of how to get started with
        Steerpath Smart SDK using react-native.
      </Text>
      <AboutItem title="Version:" value={getVersion()} />
      <AboutItem
        title="Source code:"
        value={sourceCodeUrl}
        onPress={() => {
          openLink(sourceCodeUrl);
        }}
      />
      <AboutItem
        title="Website:"
        value={steerpathUrl}
        onPress={() => {
          openLink(steerpathUrl);
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 10,
  },
  textView: {
    marginBottom: 5,
    fontSize: 14,
  },
  linkStyle: {
    color: colors.link,
    fontWeight: 'bold',
  },
});
