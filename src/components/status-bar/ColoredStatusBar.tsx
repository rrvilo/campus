/*
 * Copyright 2020 Steerpath Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import {StatusBar} from 'react-native';
import {View} from 'react-native-animatable';
import {colors} from '../../styles/colors';
import {getStatusBarHeight} from 'react-native-status-bar-height';

const ColoredStatusBar = () => {
  return (
    <View>
      <StatusBar
        barStyle="light-content"
        translucent={true}
        backgroundColor="transparent"
      />
      <View style={{width: '100%', backgroundColor: colors.brand_color}}>
        <View style={{height: getStatusBarHeight()}} />
      </View>
    </View>
  );
};

export default ColoredStatusBar;
