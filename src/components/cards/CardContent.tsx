/*
 * Copyright 2020 Steerpath Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import {View, ViewStyle} from 'react-native';

export interface CardContentProps {
  children: any;
  styles?: ViewStyle;
}

const CardContent: React.FC<CardContentProps> = ({children, styles = {}}) => (
  <View style={[{paddingVertical: 10}, styles]}>{children}</View>
);

export default CardContent;
