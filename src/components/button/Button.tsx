/*
 * Copyright 2020 Steerpath Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import {View, TouchableOpacity, StyleSheet, Text} from 'react-native';
import Typography from '../typography/Typography';
import {colors} from '../../styles/colors';

export interface ButtonProps {
  onPress: () => void;
  iconComponent?: React.ReactNode;
  buttonText: string;
  isWhiteText: boolean;
  buttonBackground?: string;
}

const Button = ({
  onPress,
  iconComponent,
  buttonText,
  buttonBackground,
  isWhiteText,
}: ButtonProps) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        styles.container,
        {
          flexDirection: iconComponent ? 'row' : 'column',
          justifyContent: iconComponent ? 'flex-start' : 'center',
          alignItems: 'center',
          backgroundColor: buttonBackground || colors.button_background,
          borderRadius: 10,
          padding: 10,
        },
      ]}>
      {iconComponent && iconComponent}

      <Text
        style={{
          textAlign: 'center',
          flex: 1,
          flexWrap: 'wrap',
          marginLeft: iconComponent ? 10 : 0,
        }}>
        <Typography
          variant="body1"
          color={isWhiteText ? 'brand_content_on_brand' : 'link'}>
          {buttonText.toUpperCase()}
        </Typography>
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 'auto',
    width: '100%',
  },
});

export default Button;
