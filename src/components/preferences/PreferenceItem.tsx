/*
 * Copyright 2020 Steerpath Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import * as React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import {darkSlateBlue, colors} from '../../styles/colors';
import {baseFontSize, smallFontSize} from '../../styles/typography';
import Typography from '../typography/Typography';

interface PreferenceListItemProps {
  preferenceIconName: string;
  preferenceIconType?: any;
  preferenceTitle: string;
  preferenceDescription?: string;
  onPress?: any;
  onValueChange?: any;
  customizedIcon?: any;
}

export const PreferenceListItem: React.FC<PreferenceListItemProps> = ({
  preferenceIconName,
  preferenceTitle,
  preferenceDescription = '',
  onPress = () => {},
  preferenceIconType: PreferenceIconType,
}) => {
  const ChosenIcon = PreferenceIconType || Icon;
  return (
    <TouchableOpacity style={styles.preferenceItem} onPress={onPress}>
      <View style={{flexDirection: 'row', flexWrap: 'nowrap', maxWidth: '80%'}}>
        <View style={styles.icon}>
          <ChosenIcon
            name={preferenceIconName}
            size={22}
            color={colors.white}
          />
        </View>
        <View style={styles.textContainer}>
          <Typography variant="body2" color="brand_content_on_brand">
            {preferenceTitle}
          </Typography>
          <Typography variant="body1" color="brand_content_on_brand">
            {preferenceDescription}
          </Typography>
        </View>
      </View>
      <Icon
        name="chevron-right"
        size={22}
        style={styles.arrowIcon}
        color={colors.brand_content_on_brand}
      />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  preferenceItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: colors.brand_color,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
    marginVertical: 10,
    paddingVertical: 10,
    paddingRight: 10,
    height: 'auto',
    borderRadius: 10,
  },
  icon: {
    width: 50,
    padding: 10,
    display: 'flex',
    alignItems: 'center',
  },
  textContainer: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingHorizontal: 10,
    maxWidth: '100%',
  },
  title: {
    color: colors.card_background,
    fontSize: baseFontSize,
    fontWeight: 'bold',
  },
  description: {
    color: darkSlateBlue,
    fontSize: smallFontSize,
    marginVertical: 5,
  },
  arrowIcon: {
    // marginHorizontal: 10,
  },
});
